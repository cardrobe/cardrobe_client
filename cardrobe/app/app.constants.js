'use strict';
var app = angular.module('appConstants', []);

/* appConstants is a 'constant' service used as a single place to keep all hard coded constants. */

 app.constant('appConstants', {

 		app		: 'cardrobe',
 		version : '0.1',
		WS_URL  : 'http://ec2-54-152-45-112.compute-1.amazonaws.com:32766/1/',

	strings: {

	},

	wsParams:{		

	},

	services:{
		BLOG: 'adblog',
		BLOG_SEARCH: 'adblogsearch',
		BLOG_COMMENT: 'adblogcomment',

		BRANDLINK: 'brandlink',
		DEALERLINK: 'dealerlink',
		FINANCIERLINK: 'financierlink',
		INSURERLINK: 'insurerlink',

		DEALERDETAILS: 'dealerdetails',
		FINANCIERDETAILS: 'financierdetails',
		INSURERDETAILS: 'insurerdetails',

		PRODUCT: 'product',
		RECOMMENDED: 'recommended',
		FEATURES: 'features',		

	}

});