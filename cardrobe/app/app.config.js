'use strict';

/* Route configuration for the FarmLogics */

app.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {

        // Application routes
        $routeProvider
            .when('/404', {
                templateUrl: 'app/components/shared/404.html'

            })

            .when('/login', {
                templateUrl: 'app/components/login/login.html',
                controller: 'LoginCtrl'

            })

            .when('/home', {
                templateUrl: 'app/components/home/home.html',
                controller: 'HomeCtrl'

            })

            .when('/blog', {
                templateUrl: 'app/components/blog/blog.html',
                controller: 'BlogCtrl'

            })

            .when('/blog_details', {
                templateUrl: 'app/components/blog/blog_details.html',
                controller: 'BlogCtrl'

            })

            .when('/contact', {
                templateUrl: 'app/components/contact/contact.html',
                controller: 'ContactCtrl'

            })

            .when('/product_details', {
                templateUrl: 'app/components/productdetails/product_details.html',
                controller: 'ProductDetailsCtrl'

            })

            .when('/cart_items', {
                templateUrl: 'app/components/cartitems/cart_items.html',
                controller: 'CartItemsCtrl'

            })

            .when('/wish_items', {
                templateUrl: 'app/components/wishitems/wish_items.html',
                controller: 'WishItemsCtrl'

            })

            .when('/compare_items', {
                templateUrl: 'app/components/compareitems/compare_items.html',
                controller: 'CompareItemsCtrl'

            })

            .when('/dealer', {
                templateUrl: 'app/components/dealer/dealer.html',
                controller: 'DealerCtrl'

            })

            .when('/financier', {
                templateUrl: 'app/components/financier/financier.html',
                controller: 'FinancierCtrl'

            })

            .when('/insurer', {
                templateUrl: 'app/components/insurer/insurer.html',
                controller: 'InsurerCtrl'

            })

            .when('/find_a_car', {
                templateUrl: 'app/components/findacar/findacar.html',
                controller: 'FindCtrl'

            })

            .when('/about_us', {
                templateUrl: 'app/components/about/about.html',

            })

            .when('/emi_calculator', {
                templateUrl: 'app/components/emicalculator/emi_calculator.html',
                controller: 'EmiCtrl'

            })

            .otherwise({
                redirectTo: '/home'
            })            

            // $locationProvider.html5Mode(true);

        }

        

])

// .run(['$location','$rootScope','localStorageService', '$q', function($location, $rootScope, localStorageService, $q){

//     var recentArray = [];
//     // recent pages stored to localstorage
//     $rootScope.$on('scanner-started', function(event, args) {
//         recentArray.push(args);
//         localStorageService.set("recent",recentArray);
//         $rootScope.recentsPage();

//     });

//     $rootScope.$on('$locationChangeStart', function (event, next, current) {
//         // redirect to login page if not logged in and trying to access a restricted page
//         var restrictedPage = $.inArray($location.path(), ['/login', '/cookie']) === -1;

//         var loggedIn = localStorage.getItem('sid');
//         //Restrict access to secure page if not already logged in
//         if (restrictedPage && !loggedIn) {
//             $location.path('/login');
//             return;
//         }

//         //Restrict login page if already logged in
//         if (loggedIn && $.inArray($location.path(), ['/login']) != -1){
//             $location.path('/dashboard');
//             return;
//         }

//     });

//     var deferred = $q.defer();

//     $rootScope.$on("$locationChangeSuccess", function (event, current, previous) {
//         if(localStorage.getItem('sid')){
//             var previouspage = previous;
//             localStorageService.set("prepage",previouspage);

//         }
//     });

// }]);