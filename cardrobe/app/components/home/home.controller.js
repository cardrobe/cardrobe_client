(function() {
    'use strict';

    app.controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices'];

    function HomeCtrl($scope, $rootScope, $location, $http, appConstants, commonservices) {
    	$scope.products = {};

        commonservices.GetProduct().then(function successCallback(products) {
            $scope.products = products;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

	}

})();