'use strict';

app.factory('blogservice', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
    	GetBlogs: function () {
        	var apiURL = appConstants.WS_URL + appConstants.services.BLOG_SEARCH;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "text/plain"
                }

            }).then(function successCallback(response) {
                return response.data.result;
                
            }, function errorCallback(response) {
                console.log(response);
                $rootScope.responseError(response);
            });
        },
        
        GetBlogDetails: function (id) {
        	var apiURL = appConstants.WS_URL + appConstants.services.BLOG + "/" + id;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "text/plain"
                }

            }).then(function successCallback(response) {
                return response.data.result;
                
            }, function errorCallback(response) {
                console.log(response);
                $rootScope.responseError(response);
            });
        },

        PostComment: function (comment) {
        	var apiURL = appConstants.WS_URL + appConstants.services.BLOG_COMMENT;
            console.log(apiURL)
            return $http({
                method: "POST",
                url: apiURL,
                data: comment,
                headers: {
                    "Content-Type": "text/plain"
                }
            }).then(function successCallback(response) {
                return response;

            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },


    }

}]);