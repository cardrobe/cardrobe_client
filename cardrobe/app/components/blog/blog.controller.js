(function() {
    'use strict';

    app.controller('BlogCtrl', BlogCtrl);

    BlogCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices', 'blogservice'];

    function BlogCtrl($scope, $rootScope, $location, $http, appConstants, commonservices, blogservice) {

    	$scope.postcomment = {};

    	var init = function () {

	        blogservice.GetBlogs().then(function successCallback(blogs) {
	          	$scope.blogs = blogs;
	          	console.log($scope.blogs)
	          
	        }, function errorCallback(response) {
	          	$rootScope.responseError(response);

	        });	      

	    };
	    init();

	    $scope.getBlogDetails = function(val) {	    	
	    	var blogID = val;
		    blogservice.GetBlogDetails(blogID).then(function successCallback(blogdetails) {
		    	$location.path("/blog_details");          
		      	$scope.blogdetails = blogdetails;
		      	$scope.blogcomments = blogdetails;

		    }, function errorCallback(response) {
		      	console.log(response);
		      	$rootScope.responseError(response);

		    });

     	}

     	$scope.postComment = function() {	    	
	    	
		    blogservice.PostComment($scope.postcomment).then(function successCallback(blogdetails) {
		    	// $location.path("/blog_details");          
		     //  	$scope.blogdetails = blogdetails;
		     //  	$scope.blogcomments = blogdetails;

		    }, function errorCallback(response) {
		      	console.log(response);
		      	$rootScope.responseError(response);

		    });

     	}  	

	}

})();