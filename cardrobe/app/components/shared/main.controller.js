(function() {
    'use strict';

    app.controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$scope', '$rootScope', '$location','localStorageService', '$http', 'appConstants', 'commonservices'];

    function MainCtrl($scope, $rootScope, $location, localStorageService, $http, appConstants, commonservices) {

        $scope.products = {};

        commonservices.GetProduct().then(function successCallback(products) {
            $scope.products = products;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

    	$scope.features_items = {};

    	commonservices.GetFeaturesItems().then(function successCallback(product) {
        	$scope.features_items = product;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });
    	
    	$scope.fitems_qty = 6;

        $scope.recommended_items = {};

        commonservices.GetRecommendedItems().then(function successCallback(product) {
            $scope.recommended_items = product;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });
        
        $scope.ritems_qty = 6;

        $scope.brands = {};
        commonservices.GetBrandLink().then(function successCallback(brands) {
            $scope.brands = brands;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

        $scope.dealers = {};
        commonservices.GetDealerLink().then(function successCallback(dealers) {
            $scope.dealers = dealers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

        $scope.financiers = {};
        commonservices.GetFinancierLink().then(function successCallback(financiers) {
            $scope.financiers = financiers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

        $scope.insurers = {};
        commonservices.GetInsurerLink().then(function successCallback(insurers) {
            $scope.insurers = insurers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

        var cartArray = [];
        var wishlistArray = [];
        var compareArray = [];

        $scope.AddtoCart = function (val){

            if (localStorage.getItem('sid')) {
                
            }else{
                cartArray.push(val);
                localStorageService.set("cart",cartArray);

            };

        }

        $scope.AddtoWishlist = function (val){
            if (localStorage.getItem('sid')) {
                
            }else{
                wishlistArray.push(val);
                localStorageService.set("wishlist",wishlistArray);

            };
        }

        $scope.AddtoCompare = function (val){
            if (localStorage.getItem('sid')) {
                
            }else{
                compareArray.push(val);
                localStorageService.set("compare",compareArray);

            };
        }

	}

})();

// filter for removing duplication
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [],
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});