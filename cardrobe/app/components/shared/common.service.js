'use strict';

app.factory('commonservices', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
        // service for retrieve features items
        GetProduct: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.PRODUCT;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    	// service for retrieve features items
        GetFeaturesItems: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.FEATURES;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

        // service for retrieve recommended items
        GetRecommendedItems: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.RECOMMENDED;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

        // service for retrieve brand lookup
        GetBrandLink: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.BRANDLINK;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

        // service for retrieve dealer lookup
        GetDealerLink: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.DEALERLINK;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

        // service for retrieve finance lookup
        GetFinancierLink: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.FINANCIERLINK;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

        // service for retrieve insurance lookup
        GetInsurerLink: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.INSURERLINK;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    }

}]);