(function() {
    'use strict';

    app.controller('CartItemsCtrl', CartItemsCtrl);

    CartItemsCtrl.$inject = ['$scope', '$rootScope', '$location', 'localStorageService', '$http', 'appConstants', 'commonservices', 'cartitemsservices'];

    function CartItemsCtrl($scope, $rootScope, $location, localStorageService, $http, appConstants, commonservices, cartitemsservices) {
        
        $scope.cartitem = {};

        $scope.getCartArray = function () {            
            $scope.cartitem = localStorageService.get("cart");
        };
    	
        if (localStorage.getItem('sid')) {
             $scope.cartitems = {};

            cartitemsservices.GetCartItems().then(function successCallback(product) {
                $scope.cartitems = product;

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });
        
            $scope.citems_qty = 6;

        }else{
            $scope.products = {};
            $scope.cartitems = [];

            commonservices.GetProduct().then(function successCallback(products) {
                $scope.products = products;
                $scope.getCartArray();
                if ($scope.cartitem.length != 0 ) {
                    for (var i = 0; i < $scope.cartitem.length; i++) {
                        for (var j = 0; j < $scope.products.length; j++) {
                            var cartitemID = Number($scope.cartitem[i]);
                            
                            if ( cartitemID == $scope.products[j].idtbadproduct) {
                                $scope.cartitems.push({
                                    "imageurl": $scope.products[j].imageurl,
                                    "model": $scope.products[j].model,
                                    "idtbadproduct": Number($scope.products[j].idtbadproduct),
                                    "price": $scope.products[j].price

                                });
                            };
                        };
                    };
                };

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });

        };

	}

})();