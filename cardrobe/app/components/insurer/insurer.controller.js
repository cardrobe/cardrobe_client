(function() {
    'use strict';

    app.controller('InsurerCtrl', InsurerCtrl);

    InsurerCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices', 'insurerservice'];

    function InsurerCtrl($scope, $rootScope, $location, $http, appConstants, commonservices, insurerservice) {
        $scope.insurers = {};

        insurerservice.GetInsurerDetails().then(function successCallback(insurers) {
            $scope.insurers = insurers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });
    }

})();