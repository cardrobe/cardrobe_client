'use strict';

app.factory('insurerservice', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
        // service for retrieve insurer details
        GetInsurerDetails: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.INSURERDETAILS;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    }

}]);