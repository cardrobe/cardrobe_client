(function() {
    'use strict';

    app.controller('WishItemsCtrl', WishItemsCtrl);

    WishItemsCtrl.$inject = ['$scope', '$rootScope', '$location', 'localStorageService', '$http', 'appConstants', 'commonservices', 'wishitemsservices'];

    function WishItemsCtrl($scope, $rootScope, $location, localStorageService, $http, appConstants, commonservices, wishitemsservices) {
    	
        $scope.wishlistitem = {};

        $scope.getWishlistArray = function () {            
            $scope.wishlistitem = localStorageService.get("wishlist");
        };
        
        if (localStorage.getItem('sid')) {
             $scope.wishitems = {};

            wishitemsservices.GetWishItems().then(function successCallback(product) {
                $scope.wishitems = product;

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });
            
            $scope.witems_qty = 6;


        }else{
            $scope.products = {};
            $scope.wishlistitems = [];

            commonservices.GetProduct().then(function successCallback(products) {
                $scope.products = products;
                $scope.getWishlistArray();
                if ($scope.wishlistitem.length != 0 ) {
                    for (var i = 0; i < $scope.wishlistitem.length; i++) {
                        for (var j = 0; j < $scope.products.length; j++) {
                            var wishlistitemID = Number($scope.wishlistitem[i]);
                            
                            if ( wishlistitemID == $scope.products[j].idtbadproduct) {
                                $scope.wishlistitems.push({
                                    "imageurl": $scope.products[j].imageurl,
                                    "model": $scope.products[j].model,
                                    "idtbadproduct": Number($scope.products[j].idtbadproduct),
                                    "price": $scope.products[j].price

                                });
                            };
                        };
                    };
                };

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });

        };

	}

})();