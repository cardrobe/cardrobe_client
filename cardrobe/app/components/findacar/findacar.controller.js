(function() {
    'use strict';

    app.controller('FindCtrl', FindCtrl);

    FindCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices'];

    function FindCtrl($scope, $rootScope, $location, $http, appConstants, commonservices) {

    	// $scope.product = dataFactory.query();

    	$scope.productsDetailed = {};
    	$scope.product = {};

    	commonservices.GetProduct().then(function successCallback(products) {
            $scope.productsDetailed = products;
            $scope.product = products;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });
    	
		$scope.priceSlider = {
		    priceMin: 1,
		    priceMax: 5000000,
		    options: {
		      ceil: 10000000,
		      floor: 0,
		      id: 'translate-slider',
		      translate: function(value, id, which) {
		        console.info(value, id, which);
		        return '&#x20a8; ' + value + ' K';
		      }
		    }
		};

		$scope.powerSlider = {
		    powerMin: 1,
		    powerMax: 200,
		    options: {
		      ceil: 500,
		      floor: 0,
		      id: 'translate-slider',
		      translate: function(value, id, which) {
		        console.info(value, id, which);
		        return value + ' kW';
		      }
		    }
		};

		$scope.capacitySlider = {
		    capacityMin: 1,
		    capacityMax: 4,
		    options: {
		      ceil: 10,
		      floor: 0,
		      id: 'translate-slider',
		      translate: function(value, id, which) {
		        console.info(value, id, which);
		        return value + 'Seat';
		      }
		    }
		};

		$scope.milegeSlider = {
		    milegeMin: 1,
		    milegeMax: 20,
		    options: {
		      ceil: 25,
		      floor: 0,
		      id: 'translate-slider',
		      translate: function(value, id, which) {
		        console.info(value, id, which);
		        return value + ' km';
		      }
		    }
		};

		$scope.sliderRanges = {
		    priceMin: 1,
		    priceMax: 50,
		    powerMin: 1,
		    powerMax: 200,
		    capacityMin: 1,
		    capacityMax: 4,
		    milegeMin: 1,
		    milegeMax: 20

		};		

	}

})();

app.filter('rangeFilter', function() {
  return function(items, sliderRanges ) {
  	console.log(items)
    var filtered = [];
    var priceMin = parseInt(sliderRanges.priceMin);
    var priceMax = parseInt(sliderRanges.priceMax);
    var powerMin = parseInt(sliderRanges.powerMin);
    var powerMax = parseInt(sliderRanges.powerMax);
    var capacityMin = parseInt(sliderRanges.capacityMin);
    var capacityMax = parseInt(sliderRanges.capacityMax);
    var milegeMin = parseInt(sliderRanges.milegeMin);
    var milegeMax = parseInt(sliderRanges.milegeMax);

    angular.forEach(items, function(item) {

      if((item.price >= priceMin && item.price <= priceMax) && (item.power >= powerMin && item.power <= powerMax) && (item.capacity >= capacityMin && item.capacity <= capacityMax) && (item.milege >= milegeMin && item.milege <= milegeMax)) {
        filtered.push(item);
      }
    });
    
    return filtered;
  };
});