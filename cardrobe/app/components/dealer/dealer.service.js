'use strict';

app.factory('dealerservice', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
    	// service for retrieve dealer details
        GetDealerDetails: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.DEALERDETAILS;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    }

}]);