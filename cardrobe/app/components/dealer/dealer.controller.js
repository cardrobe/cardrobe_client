(function() {
    'use strict';

    app.controller('DealerCtrl', DealerCtrl);

    DealerCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices', 'dealerservice'];

    function DealerCtrl($scope, $rootScope, $location, $http, appConstants, commonservices, dealerservice) {
    	$scope.dealers = {};

    	dealerservice.GetDealerDetails().then(function successCallback(dealers) {
        	$scope.dealers = dealers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });
	}

})();