'use strict';

app.factory('compareitemsservices', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
    	// service for retrieve languages
        GetCompareItems: function(){
            return $http({
                method: "GET",
                url: 'assets/json/product.json',
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    }

}]);