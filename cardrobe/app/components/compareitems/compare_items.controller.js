(function() {
    'use strict';

    app.controller('CompareItemsCtrl', CompareItemsCtrl);

    CompareItemsCtrl.$inject = ['$scope', '$rootScope', '$location', 'localStorageService', '$http', 'appConstants', 'commonservices', 'compareitemsservices'];

    function CompareItemsCtrl($scope, $rootScope, $location, localStorageService, $http, appConstants, commonservices, compareitemsservices) {

        $scope.compareitem = {};

        $scope.getCompareArray = function () {            
            $scope.compareitem = localStorageService.get("compare");
        };
        
        if (localStorage.getItem('sid')) {
            $scope.compareitems = {};

            compareitemsservices.GetCompareItems().then(function successCallback(product) {
                $scope.compareitems = product;

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });
            
            $scope.citems_qty = 6;


        }else{
            $scope.products = {};
            $scope.compareitems = [];

            commonservices.GetProduct().then(function successCallback(products) {
                $scope.products = products;
                $scope.getCompareArray();
                if ($scope.compareitem.length != 0 ) {
                    for (var i = 0; i < $scope.compareitem.length; i++) {
                        for (var j = 0; j < $scope.products.length; j++) {
                            var cartitemID = Number($scope.compareitem[i]);
                            
                            if ( cartitemID == $scope.products[j].idtbadproduct) {
                                $scope.compareitems.push({
                                    "imageurl": $scope.products[j].imageurl,
                                    "model": $scope.products[j].model,
                                    "idtbadproduct": Number($scope.products[j].idtbadproduct),
                                    "price": $scope.products[j].price

                                });
                            };
                        };
                    };
                };

            }, function errorCallback(response) {
              console.log(response);
              $rootScope.responseError(response);

            });

        };

	}

})();