'use strict';

app.factory('financierservice', ['$rootScope', '$http', 'appConstants','$q' , function ($rootScope, $http, appConstants, $q) {

    return {
        // service for retrieve financier details
        GetFinancierDetails: function(){
            var apiURL = appConstants.WS_URL + appConstants.services.FINANCIERDETAILS;
            console.log(apiURL)
            return $http({
                method: "GET",
                url: apiURL,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                    return response.data.result;
            }, function errorCallback(response) {
                $rootScope.responseError(response);
            });
        },

    }

}]);