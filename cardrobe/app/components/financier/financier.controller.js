(function() {
    'use strict';

    app.controller('FinancierCtrl', FinancierCtrl);

    FinancierCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices', 'financierservice'];

    function FinancierCtrl($scope, $rootScope, $location, $http, appConstants, commonservices, financierservice) {
        $scope.financiers = {};

        financierservice.GetFinancierDetails().then(function successCallback(financiers) {
            $scope.financiers = financiers;

        }, function errorCallback(response) {
          console.log(response);
          $rootScope.responseError(response);

        });

        $scope.rangeSlider = {
            minValue: 10,
            maxValue: 90,
            options: {
              floor: 0,
              ceil: 100,
              step: 1
            }
        };
        
    }

})();