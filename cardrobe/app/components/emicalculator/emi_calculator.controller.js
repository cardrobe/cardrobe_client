(function() {
    'use strict';

    app.controller('EmiCtrl', EmiCtrl);

    EmiCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', 'appConstants', 'commonservices'];

    function EmiCtrl($scope, $rootScope, $location, $http, appConstants, commonservices) {
    	$scope.loanAmountSlider = {
		    value: 1000000

		};

		$scope.downPaymentSlider = {
		    value: 2500

		};

		$scope.numberMonthSlider = {
		    value: 36
		};

		$scope.interestRateSlider = {
		    value: 15
		};

		$scope.amount = {};

		$scope.emiCalculation = function(){
			var principalAmount = Number($scope.loanAmountSlider.value) - Number($scope.downPaymentSlider.value);
			var princ = principalAmount;
			var term  = $scope.numberMonthSlider.value;
			var intr  = $scope.interestRateSlider.value / 1200;
			var emi = princ * intr / (1 - (Math.pow(1/(1 + intr), term)));
			var payable = emi * $scope.numberMonthSlider.value;
			var interest = payable - principalAmount;

			$scope.amount.emi = emi.toFixed(2);
			$scope.amount.payable = payable.toFixed(2);
			$scope.amount.interest = interest.toFixed(2);	 

		}

		$scope.emiCalculation();		

	}

})();

